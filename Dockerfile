FROM frolvlad/alpine-glibc:latest

ENV IVY_VERSION 2.4.0
ENV IVY_SHA256 "7a3d13a80b69d71608191463dfc2a74fff8ef638ce0208e70d54d28ba9785ee9  apache-ivy-2.4.0-bin.tar.gz"
ENV JSCH_VERSION 0.1.54
ENV JSCH_SHA256 "ef01316ee08bc02f393f345ff10e58907f49c9f324f3420c03e71a58ffc4013f  jsch-0.1.54.jar"
ENV ARTIFACTS /tmp/image-artifacts

RUN apk add --no-cache \
    apache-ant \
    git \
    openjdk8 \
    openssl

RUN apk add --no-cache \
    binutils \
    gcc \
    make \
    libc-dev

RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/main --allow-untrusted \
    libexecinfo \
    libexecinfo-dev

RUN apk add --no-cache \
    curl

WORKDIR $ARTIFACTS
# Ivy is not in the official repos. Get binary and manually install.
RUN wget ftp://apache.mirrors.tds.net/pub/apache.org//ant/ivy/$IVY_VERSION/apache-ivy-$IVY_VERSION-bin.tar.gz && \
    echo "$IVY_SHA256" | sha256sum -c && \
    tar -xzf apache-ivy-$IVY_VERSION-bin.tar.gz && \
    mv apache-ivy-$IVY_VERSION/*.jar /usr/share/java/apache-ant/lib/

# Ant ships with a Jsch lib that does not support SSH or SCP (???)
RUN wget https://sourceforge.net/projects/jsch/files/jsch.jar/$JSCH_VERSION/jsch-$JSCH_VERSION.jar && \
    echo "$JSCH_SHA256" | sha256sum -c && \
    mv jsch-$JSCH_VERSION.jar /usr/share/java/apache-ant/lib/ant-jsch.jar

RUN rm -rf *